package me.italiaantjes.pc;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;

/**
 * Created by Italiaantjes on 12-8-2017.
 */
public class Util {

    public static String format(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static void createCustomItemLores(Inventory inv, Material material, byte byt, String displayName, String lore) {
        ItemStack chest2 = new ItemStack(material, 1, (byte) byt);
        ItemMeta plate2 = chest2.getItemMeta();
        plate2.setDisplayName(displayName);
        ArrayList<String> iron21 = new ArrayList<String>();
        iron21.add(lore);
        plate2.setLore(iron21);
        chest2.setItemMeta(plate2);

        inv.contains(chest2);
        inv.addItem(chest2);
    }
}
