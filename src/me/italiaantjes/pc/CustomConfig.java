package me.italiaantjes.pc;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;


/**
 * Created by Italiaantjes on 10-8-2017.
 */
public class CustomConfig {

    public static CustomConfig instance = new CustomConfig();

    private Main plugin = Main.getPlugin(Main.class);

    public HashMap<String, List<String>> achieved = new HashMap<>();

    public FileConfiguration EconomyConfig;
    public File EconomyFile;

    public void setup() {
        if (!plugin.getDataFolder().exists()) {
            plugin.getDataFolder().mkdir();
        }
        EconomyFile = new File(plugin.getDataFolder(), "achievements.yml");
        if (!EconomyFile.exists()) {
            try {
                EconomyFile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        EconomyConfig = YamlConfiguration.loadConfiguration(EconomyFile);

        if (!getConfig().contains("achieved")) {
            getConfig().createSection("achieved");
        }
        if (!getConfig().contains("achievements")) {
            getConfig().createSection("achievements");
        }
        ConfigurationSection section1 = getConfig().getConfigurationSection("achievements");
        for (String ke : section1.getKeys(false)) {
            Main.getInstance().achievements.put(ke, new CustomAchievement(ke, getConfig().getString(ke + ".name"), getConfig().getString(ke + ".desc"), getConfig().getDouble(ke + "." + "coins")));
            saveConfig();

        }
        for (String key : getConfig().getConfigurationSection("achieved").getKeys(false)) {
                achieved.put(key, getConfig().getStringList("achieved." + key));

                saveConfig();
        }
        }







    public FileConfiguration getConfig() {
        return EconomyConfig;
    }

    public void saveConfig() {
        try {
            EconomyConfig.save(EconomyFile);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static CustomConfig getInstance() {
        return instance;
    }

    public void reloadConfig() {
        EconomyConfig = YamlConfiguration.loadConfiguration(EconomyFile);
    }
    public void save() {

    }
}
