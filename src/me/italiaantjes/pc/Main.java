package me.italiaantjes.pc;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 * Created by Italiaantjes on 13-8-2017.
 */
public class Main extends JavaPlugin {

    public static Main instance;
    public HashMap<String, CustomAchievement> achievements = new HashMap<>();


    @Override
    public void onEnable() {
        if(!new AdvancedLicense("2WXI-N8KC-V9IJ-ESHN", "http://license.larsfx.nl/verify.php", this).register()) return;
        instance = this;

        Bukkit.getServer().getPluginManager().registerEvents(new ClickEvent(), this);

        getCommand("pca").setExecutor(new PCACommand());
        getCommand("achievements").setExecutor(new Menu());

        CustomConfig.getInstance().setup();
        setup();

        getConfig().options().copyDefaults(true);
        saveConfig();
    }
    public static Main getInstance() {
        return instance;
    }
    public void setup() {
        ConfigurationSection achievementsSection = CustomConfig.getInstance().getConfig().getConfigurationSection("achievements");
        Set<String> achievementNames = achievementsSection.getKeys(false);
        for(String key : achievementNames) {
            ConfigurationSection section = achievementsSection.getConfigurationSection(key);
            achievements.put(key, new CustomAchievement(key, section.getString("name"), section.getString("desc"), section.getDouble("coins")));

        }
    }
    @Override
    public void onDisable() {
        CustomConfig.getInstance().saveConfig();
    }
}
