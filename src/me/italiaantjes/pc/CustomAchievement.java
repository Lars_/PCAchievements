package me.italiaantjes.pc;

public class CustomAchievement {

    private String achievement;
    private String name;
    private String description;
    private double reward;

    public CustomAchievement(String achievement, String name, String description, double reward) {
        this.achievement = achievement;
        this.name = name;
        this.description = description;
        this.reward = reward;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public double getReward() {
        return this.reward;
    }
    public String getAchievement() {
        return this.achievement;
    }
}
