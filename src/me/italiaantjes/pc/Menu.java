package me.italiaantjes.pc;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by Italiaantjes on 23-8-2017.
 */
public class Menu implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] args) {
        if (!(commandSender instanceof Player)) {
            commandSender.sendMessage(Util.format("&cFout: Alleen een speler kan dit commando uitvoeren."));
            return false;
        } else {
            Player player = (Player) commandSender;
            Inventory inv = Bukkit.createInventory(null, 54, Util.format("&cAchievements"));
            player.openInventory(inv);
            Set<Map.Entry<String, CustomAchievement>> pairs = Main.getInstance().achievements.entrySet();
            String uuid = ((Player) commandSender).getUniqueId().toString();
            if((CustomConfig.getInstance().achieved.isEmpty())) {
            return false;
            } else {
            for (Map.Entry<String, CustomAchievement> entry : pairs) {
                String key = (String) entry.getKey();
                CustomAchievement value = (CustomAchievement) entry.getValue();

                if (((List) CustomConfig.getInstance().achieved.get(key)).contains(uuid)) {

                    Util.createCustomItemLores(inv, Material.STAINED_CLAY, (byte) 13, Util.format("&a✔ " + value.getName()), Util.format(value.getDescription()));
                } else {
                    Util.createCustomItemLores(inv, Material.STAINED_CLAY, (byte) 14, Util.format("&c✖ " + value.getName()), Util.format("&c?"));

                }


                }

            }
        }
        return false;
    }
}
