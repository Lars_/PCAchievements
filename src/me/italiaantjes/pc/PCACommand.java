package me.italiaantjes.pc;

import me.lars.pce.API;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Italiaantjes on 12-8-2017.
 */
public class PCACommand implements CommandExecutor {

    me.lars.pce.API coins = new API();

    @Override
    public boolean onCommand(CommandSender commandSender, org.bukkit.command.Command command, String s, String[] args) {
        if (!commandSender.hasPermission("pca.*")) {
            commandSender.sendMessage(Util.format("&cFout: U heeft hier geen permisie voor!"));
            return false;
        }
        if (args.length == 0) {
            commandSender.sendMessage(Util.format("&cFout: Niet genoeg argumenten!"));
            return false;
        }
        if (args.length == 5 && args[0].equalsIgnoreCase("create")) {
            if (Main.getInstance().achievements.containsKey(args[1])) {
                commandSender.sendMessage(Util.format("&cFout: Achievement bestaat al!"));
                return false;

            } else {
                String achievements = args[1];
                String name = args[2].replaceAll("_", " ");
                String desc = args[3].replaceAll("_", " ");
                Double coins = Double.parseDouble(args[4]);
                CustomAchievement achievement = new CustomAchievement(achievements, name, desc, coins);
                Main.getInstance().achievements.put(achievements, achievement);

                ConfigurationSection section = CustomConfig.getInstance().getConfig().getConfigurationSection("achievements");
                section.set(achievement.getAchievement() + ".desc", achievement.getDescription());
                section.set(achievement.getAchievement() + ".name", achievement.getName());
                section.set(achievement.getAchievement() + ".coins", achievement.getReward());
                CustomConfig.getInstance().achieved.put(achievement.getAchievement(), new ArrayList<>());
                CustomConfig.getInstance().getConfig().set(achievement.getAchievement(), new ArrayList<>());
                CustomConfig.getInstance().getConfig().set("achieved" + "." + achievement.getAchievement(), achievement.getAchievement() + "." + new ArrayList<>());
                CustomConfig.getInstance().saveConfig();
                commandSender.sendMessage(Util.format("&aU heeft succesvol een achievement aangemaakt!"));
            }
        } else if (args.length == 2 && args[0].equalsIgnoreCase("delete")) {
            String path = "achievements." + args[1];
            CustomConfig.getInstance().getConfig().set(path, null);
            CustomConfig.getInstance().saveConfig();
            Main.getInstance().achievements.remove(args[1]);
            CustomConfig.getInstance().achieved.remove(args[1]);
            commandSender.sendMessage(Util.format("&aU heeft succesvol een achievement verwijderd!"));
            return true;
        } else if (args.length == 3 && args[0].equalsIgnoreCase("give")) {
            String achievement = args[2];
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if (target == null || !Bukkit.getOnlinePlayers().contains(target)) {
                commandSender.sendMessage(Util.format("&cFout: Speler niet gevonden!"));
                return false;
            }
            if(!Main.getInstance().achievements.containsKey(args[2])) {
                commandSender.sendMessage(Util.format("&cFout: Achievement bestaat niet!"));
                return false;
            }
            if (((List)CustomConfig.getInstance().achieved.get(achievement)).contains(target.getUniqueId().toString())) {
                return false;
            } else {
                Player play = (Player) target;
                ((List)CustomConfig.getInstance().achieved.get(achievement)).add(target.getUniqueId().toString());
                commandSender.sendMessage(Util.format("&aU heeft succesvol een achievement aan &6" + target.getName() + " gegeven!"));
                CustomAchievement ach = (CustomAchievement) Main.getInstance().achievements.get(achievement);
                List<String> list = CustomConfig.getInstance().getConfig().getStringList("achieved" + "." + ach.getAchievement());
                list.add(target.getUniqueId().toString());
                CustomConfig.getInstance().getConfig().set("achieved" + "." + ach.getAchievement(), list);
                CustomConfig.getInstance().saveConfig();
                coins.depositPlayer(target, ach.getReward());
                play.sendMessage(Util.format("&7Gefeliciteerd met deze achievement! Zoek nu"));
                play.sendMessage(Util.format("&7verder, Als je durft"));
                play.sendMessage(Util.format(" "));
                play.sendMessage(Util.format("&7Achievement: " + ach.getName()));
                play.sendMessage(Util.format("&7Coins: " + ach.getReward()));
                return true;
            }

        } else if(args.length == 3 && args[0].equalsIgnoreCase("take")) {
            String achievement = args[2];
            OfflinePlayer target = Bukkit.getOfflinePlayer(args[1]);
            if (target == null || !Bukkit.getOnlinePlayers().contains(target)) {
                return false;
            }
                if(!Main.getInstance().achievements.containsKey(achievement)) {
                    commandSender.sendMessage(Util.format("&cFout: Achievement bestaat niet!"));
                    return false;
                } else {
                    ((List)CustomConfig.getInstance().achieved.get(achievement)).remove(target.getUniqueId().toString());
                    CustomAchievement ach = (CustomAchievement) Main.getInstance().achievements.get(achievement);
                    List<String> list = CustomConfig.getInstance().getConfig().getStringList("achieved" + "." + ach.getAchievement());
                    list.remove(target.getUniqueId().toString());
                    CustomConfig.getInstance().getConfig().set("achieved" + "." + ach.getAchievement(), list);
                    commandSender.sendMessage(Util.format("&aU heeft succesvol een achievement van &6" + target.getName() + " afgepakt!"));
                    return true;
                }
            }
        return false;
    }
}

