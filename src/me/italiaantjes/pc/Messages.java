package me.italiaantjes.pc;

import java.util.List;

/**
 * Created by Italiaantjes on 13-8-2017.
 */
public class Messages {

    public static List<String> ACHIEVEMENT_MESSAGE = Main.getInstance().getConfig().getStringList("achievement-message");
    public static String NO_PERMISSIONS = Main.getInstance().getConfig().getString("no-permissions");
    public static String NOT_A_PLAYER = Main.getInstance().getConfig().getString("not-a-player");
    public static String NOT_ENOUGH_ARGS = Main.getInstance().getConfig().getString("not-enough-args");
    public static String PLAYER_DOES_NOT_EXIST = Main.getInstance().getConfig().getString("player-does-not-exist");
}
